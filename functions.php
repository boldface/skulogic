<?php

/**
 * @package Boldface\SKULogic
 */
declare( strict_types = 1 );
namespace Boldface\SKULogic;

/**
 * Use the Bootstrap auto updater
 *
 * @since 0.1
 */
function updater_init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\updater_init' );

/**
 * Filter the modules list.
 *
 * @since 0.1
 *
 * @param array $modules List of modules.
 *
 * @return array The new list of modules.
 */
function modulesList( array $modules ) : array {
  $modules[] = 'googleFonts';
  $modules[] = 'widgets';
  return $modules;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\modulesList' );

/**
 * Filter the Google fonts.
 *
 * @since 0.1.5
 *
 * @param array $fonts List of fonts.
 *
 * @return array The new list of fonts.
 */
function googleFonts( array $fonts ) : array {
  $fonts[] = 'PT Sans';
  return $fonts;
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Filter the sidebars.
 *
 * @since 0.1
 *
 * @param array $modules Array of sidebars.
 *
 * @return array The new array of sidebars.
 */
function sidebars( array $sidebars ) : array {
  $sidebars[] = [
    'name'          => 'Header',
    'id'            => 'header',
    'description'   => 'Header widgets.',
    'before_widget' => '<div class="widget header-widget sku-blue bg-sku-gray %2$s">',
    'after_widget'  => '</div>'
  ];
  $sidebars[] = [
    'name'          => 'Footer',
    'id'            => 'footer',
    'description'   => 'Footer widgets.',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>'
  ];
  return $sidebars;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\sidebars' );

/**
 * Use a white background for the navigation menu.
 *
 * @since 0.1
 *
 * @param string $class The navigation class.
 *
 * @return string The modified navigation class.
 */
function navigationClass( string $class ) : string {
  return str_replace( [ 'navbar-dark', 'bg-dark' ], [ 'navbar-light', 'bg-white' ], $class );
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 * Use container-fluid for the loop class.
 *
 * @since 0.1
 *
 * @param string $class The loop class.
 *
 * @return string The modified loop class.
 */
function loopClass( string $class ) : string {
  return 'container-fluid';
}
\add_filter( 'Boldface\Bootstrap\Views\loop\class', __NAMESPACE__ . '\loopClass' );

/**
 * Use the widget for the entry header on the front page.
 *
 * @since 0.1
 *
 * @param string $entryHeader The entry header.
 *
 * @return string The modified entry header.
 */
function loop( string $loop ) : string {
  if( ! \is_active_sidebar( 'header' ) ) {
    return $loop;
  }
  ob_start();
  \dynamic_sidebar( 'header' );
  return ob_get_clean() . $loop;
}
\add_filter( 'Boldface\Bootstrap\Views\loop', __NAMESPACE__ . '\loop', 50 );

/**
 * Use the wp_enqueue_scripts hook to enqueue scripts and styles.
 *
 * @since 0.1
 */
function enqueueScripts() {
  \wp_enqueue_style(
    'skulogic-callout',
    \get_stylesheet_directory_uri() . '/assets/css/skulogic-callout.css',
    [  ]
  );
  \wp_enqueue_script(
    'skulogic-callout',
    \get_stylesheet_directory_uri() . '/assets/js/callout.js',
    [ 'jquery' ]
  );
}
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueueScripts' );

/**
 * Use the enqueue_block_editor_assets hook to enqueue editor assets.
 *
 * @since 0.1
 */
function enqueueBlockEditorAssets() {
  \wp_enqueue_script(
    'skulogic-edtior-assets',
    \get_stylesheet_directory_uri() . '/assets/js/skulogic.js',
    [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', ]
  );
}
\add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\enqueueBlockEditorAssets' );

/**
 * Return the modified footer text.
 *
 * @since 0.1
 *
 * @param string $text Footer text.
 *
 * @return string New footer text.
 */
function footerText( string $text ) : string {
  return sprintf( '&copy; %1$s SKULogic All Rights Reserved', date( 'Y' ) );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Return the modified footer class.
 *
 * @since 0.1
 *
 * @param string $text Footer class.
 *
 * @return string New footer class.
 */
function footerClass( string $class ) : string {
  $class = str_replace( [ ' fixed-bottom', ' bg-light' ], '', $class );
  $class .= ' text-center bg-white flex-column copyright';
  return $class;
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Return the modified footer wrapper class.
 *
 * @since 0.1
 *
 * @param string $text Footer wrapper class.
 *
 * @return string New footer wrapper class.
 */
function footerWrapperClass( string $class ) : string {
  return $class . ' justify-content-center';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\wrapper\class', __NAMESPACE__ . '\footerWrapperClass' );

/**
 * Filter the footer and append an aside.
 *
 * @since 0.1
 *
 * @param string $text Footer.
 *
 * @return string New footer.
 */
function footer( string $footer ) : string {
  if( ! \is_active_sidebar( 'footer' ) ) {
    return $footer;
  }
  ob_start();
  \dynamic_sidebar( 'footer' );
  $footerWidgets = ob_get_clean();

  $widgets = sprintf(
    '<aside class="widgets footer-widget bg-sku-yellow"><div class="container">%1$s</div></aside>',
    $footerWidgets
  );
  return str_replace( '</footer>', '</footer>' . $widgets, $footer );
}
\add_filter( 'Boldface\Bootstrap\Views\footer', __NAMESPACE__ . '\footer', 20 );

/**
 * Disable the autorow feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\Controllers\entry\autorow', '__return_false' );

/**
 * Disable the REST feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );
