module.exports = {
	entry: './assets/js/block.js',
	output: {
		path: __dirname,
		filename: 'assets/js/skulogic.js',
	},
	module: {
		loaders: [
			{
				test: /.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
			},
		],
	},
};
