const { registerBlockType } = wp.blocks;
const { InspectorControls, InnerBlocks, RichText, MediaUpload, PanelColor } = wp.editor;
const { Button } = wp.components;
const { Fragment } = wp.element;

registerBlockType( 'skulogic/callout', {
	title: 'Callout',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		content: {
			type: 'array',
			source: 'children',
			selector: 'p',
		}
	},
	edit( { attributes, className, setAttributes } ) {
		const { title, content } = attributes;
		const onChangeTitle = newTitle => {
			setAttributes( { title: newTitle } );
		};
		const onChangeContent = newContent => {
			setAttributes( { content: newContent } );
		};

		return (
			<div className={ className }>
				<RichText
					tagName="h2"
					placeholder={ 'Page Header' }
					value={ title }
					onChange={ onChangeTitle }
				/>
				<RichText
					tagName="p"
					placeholder={ 'Page Content' }
					value={ content }
					onChange={ onChangeContent }
				/>
			</div>
		);
	},
	save( { attributes, className } ) {
		const { title, content } = attributes;
		return (
			<div className={ className }>
				<div className={ 'callout' }>
					<h2>{ title }</h2>
					<p>{ content }</p>
				</div>
			</div>
		);
	}
} );

registerBlockType( 'skulogic/call-to-action', {
	title: 'Call to Action',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		content: {
			type: 'array',
			source: 'children',
			selector: 'p',
		},
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
	},
	edit( { attributes, className, setAttributes } ) {
		const { mediaID, mediaURL, title, content } = attributes;
		const onChangeTitle = newTitle => {
			setAttributes( { title: newTitle } );
		};
		const onChangeContent = newContent => {
			setAttributes( { content: newContent } );
		};
		const onSelectVideo = media => {
			setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
			} );
		};

		return (
			<div className={ className }>
				<RichText
					tagName="h2"
					placeholder={ 'Page Header' }
					value={ title }
					onChange={ onChangeTitle }
				/>
				<RichText
					tagName="p"
					placeholder={ 'Page Content' }
					value={ content }
					onChange={ onChangeContent }
				/>
				<MediaUpload
					onSelect={ onSelectVideo }
					type="image"
					value={ mediaID }
					render={ ( { open } ) => (
						<Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
							{ ! mediaID ? 'Select Image' : <img src={ mediaURL } /> }
						</Button>
					) }
				/>
			</div>
		);
	},
	save( { attributes, className } ) {
		const { mediaID, mediaURL, title, content } = attributes;
		return (
			<div className={ className }>
				<img src={ mediaURL } />
				<h2>{ title }</h2>
				<p>{ content }</p>
			</div>
		);
	}
} );

registerBlockType( 'skulogic/page-header', {
	title: 'Header',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		content: {
			type: 'array',
			source: 'children',
			selector: 'p',
		},
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'div',
			attribute: 'data-src',
		},
	},
	edit( { attributes, className, setAttributes } ) {
		const { mediaID, mediaURL, title, content } = attributes;
		const onChangeTitle = newTitle => {
			setAttributes( { title: newTitle } );
		};
		const onChangeContent = newContent => {
			setAttributes( { content: newContent } );
		};
		const onSelectVideo = media => {
			setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
			} );
		};

		return (
			<div className={ className }>
				<RichText
					tagName="h2"
					placeholder={ 'Page Header' }
					value={ title }
					onChange={ onChangeTitle }
				/>
				<RichText
					tagName="p"
					placeholder={ 'Page Content' }
					value={ content }
					onChange={ onChangeContent }
				/>
				<MediaUpload
					onSelect={ onSelectVideo }
					type="image"
					value={ mediaID }
					render={ ( { open } ) => (
						<Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
							{ ! mediaID ? 'Select Image' : <img src={ mediaURL } /> }
						</Button>
					) }
				/>
			</div>
		);
	},
	save( { attributes, className } ) {
		const { mediaID, mediaURL, title, content } = attributes;

		const divStyle = {};
		if( mediaURL ) {
			divStyle.backgroundImage = 'URL('+mediaURL+')';
		}

		return (
			<div className={ className } data-src={ mediaURL } style={ divStyle }>
				<h2>{ title }</h2>
				<p>{ content }</p>
			</div>
		);
	}
} );

registerBlockType( 'skulogic/container', {
	title: 'Container',
	description: 'A block that contains other blocks.',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		backgroundColor: {
			type: 'string',
			default: '#ffffff',
		},
		textColor: {
			type: 'string',
			default: '#000000',
		},
	},
	edit( { attributes, className, setAttributes } ) {
		const { backgroundColor, textColor } = attributes;
		const setBackgroundColor = newBackgroundColor => {
			setAttributes( { backgroundColor: newBackgroundColor } );
		};
		const setTextColor = newTextColor => {
			setAttributes( { textColor: newTextColor } );
		};
		return (
			<Fragment>
				<InspectorControls>
					<PanelColor
						colorValue={ backgroundColor }
						initialOpen={ false }
						title={ 'Background Color' }
						onChange={ setBackgroundColor }
					/>
					<PanelColor
						colorValue={ textColor }
						initialOpen={ false }
						title={ 'Text Color' }
						onChange={ setTextColor }
					/>
				</InspectorControls>
				<div className={ className } style={ { backgroundColor: backgroundColor, color: textColor, } } >
				<InnerBlocks/>
				</div>
			</Fragment>
		);
	},
	save( { attributes, className } ) {
		const { backgroundColor, textColor } = attributes;
		return (
			<div className={ className } style={ { backgroundColor: backgroundColor, color: textColor, } }>
				<div class="container">
					<InnerBlocks.Content />
				</div>
			</div>
		);
	}
} );
