/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var registerBlockType = wp.blocks.registerBlockType;
var _wp$editor = wp.editor,
    InspectorControls = _wp$editor.InspectorControls,
    InnerBlocks = _wp$editor.InnerBlocks,
    RichText = _wp$editor.RichText,
    MediaUpload = _wp$editor.MediaUpload,
    PanelColor = _wp$editor.PanelColor;
var Button = wp.components.Button;
var Fragment = wp.element.Fragment;


registerBlockType('skulogic/callout', {
	title: 'Callout',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2'
		},
		content: {
			type: 'array',
			source: 'children',
			selector: 'p'
		}
	},
	edit: function edit(_ref) {
		var attributes = _ref.attributes,
		    className = _ref.className,
		    setAttributes = _ref.setAttributes;
		var title = attributes.title,
		    content = attributes.content;

		var onChangeTitle = function onChangeTitle(newTitle) {
			setAttributes({ title: newTitle });
		};
		var onChangeContent = function onChangeContent(newContent) {
			setAttributes({ content: newContent });
		};

		return wp.element.createElement(
			'div',
			{ className: className },
			wp.element.createElement(RichText, {
				tagName: 'h2',
				placeholder: 'Page Header',
				value: title,
				onChange: onChangeTitle
			}),
			wp.element.createElement(RichText, {
				tagName: 'p',
				placeholder: 'Page Content',
				value: content,
				onChange: onChangeContent
			})
		);
	},
	save: function save(_ref2) {
		var attributes = _ref2.attributes,
		    className = _ref2.className;
		var title = attributes.title,
		    content = attributes.content;

		return wp.element.createElement(
			'div',
			{ className: className },
			wp.element.createElement(
				'div',
				{ className: 'callout' },
				wp.element.createElement(
					'h2',
					null,
					title
				),
				wp.element.createElement(
					'p',
					null,
					content
				)
			)
		);
	}
});

registerBlockType('skulogic/call-to-action', {
	title: 'Call to Action',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2'
		},
		content: {
			type: 'array',
			source: 'children',
			selector: 'p'
		},
		mediaID: {
			type: 'number'
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src'
		}
	},
	edit: function edit(_ref3) {
		var attributes = _ref3.attributes,
		    className = _ref3.className,
		    setAttributes = _ref3.setAttributes;
		var mediaID = attributes.mediaID,
		    mediaURL = attributes.mediaURL,
		    title = attributes.title,
		    content = attributes.content;

		var onChangeTitle = function onChangeTitle(newTitle) {
			setAttributes({ title: newTitle });
		};
		var onChangeContent = function onChangeContent(newContent) {
			setAttributes({ content: newContent });
		};
		var onSelectVideo = function onSelectVideo(media) {
			setAttributes({
				mediaURL: media.url,
				mediaID: media.id
			});
		};

		return wp.element.createElement(
			'div',
			{ className: className },
			wp.element.createElement(RichText, {
				tagName: 'h2',
				placeholder: 'Page Header',
				value: title,
				onChange: onChangeTitle
			}),
			wp.element.createElement(RichText, {
				tagName: 'p',
				placeholder: 'Page Content',
				value: content,
				onChange: onChangeContent
			}),
			wp.element.createElement(MediaUpload, {
				onSelect: onSelectVideo,
				type: 'image',
				value: mediaID,
				render: function render(_ref4) {
					var open = _ref4.open;
					return wp.element.createElement(
						Button,
						{ className: mediaID ? 'image-button' : 'button button-large', onClick: open },
						!mediaID ? 'Select Image' : wp.element.createElement('img', { src: mediaURL })
					);
				}
			})
		);
	},
	save: function save(_ref5) {
		var attributes = _ref5.attributes,
		    className = _ref5.className;
		var mediaID = attributes.mediaID,
		    mediaURL = attributes.mediaURL,
		    title = attributes.title,
		    content = attributes.content;

		return wp.element.createElement(
			'div',
			{ className: className },
			wp.element.createElement('img', { src: mediaURL }),
			wp.element.createElement(
				'h2',
				null,
				title
			),
			wp.element.createElement(
				'p',
				null,
				content
			)
		);
	}
});

registerBlockType('skulogic/page-header', {
	title: 'Header',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2'
		},
		content: {
			type: 'array',
			source: 'children',
			selector: 'p'
		},
		mediaID: {
			type: 'number'
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'div',
			attribute: 'data-src'
		}
	},
	edit: function edit(_ref6) {
		var attributes = _ref6.attributes,
		    className = _ref6.className,
		    setAttributes = _ref6.setAttributes;
		var mediaID = attributes.mediaID,
		    mediaURL = attributes.mediaURL,
		    title = attributes.title,
		    content = attributes.content;

		var onChangeTitle = function onChangeTitle(newTitle) {
			setAttributes({ title: newTitle });
		};
		var onChangeContent = function onChangeContent(newContent) {
			setAttributes({ content: newContent });
		};
		var onSelectVideo = function onSelectVideo(media) {
			setAttributes({
				mediaURL: media.url,
				mediaID: media.id
			});
		};

		return wp.element.createElement(
			'div',
			{ className: className },
			wp.element.createElement(RichText, {
				tagName: 'h2',
				placeholder: 'Page Header',
				value: title,
				onChange: onChangeTitle
			}),
			wp.element.createElement(RichText, {
				tagName: 'p',
				placeholder: 'Page Content',
				value: content,
				onChange: onChangeContent
			}),
			wp.element.createElement(MediaUpload, {
				onSelect: onSelectVideo,
				type: 'image',
				value: mediaID,
				render: function render(_ref7) {
					var open = _ref7.open;
					return wp.element.createElement(
						Button,
						{ className: mediaID ? 'image-button' : 'button button-large', onClick: open },
						!mediaID ? 'Select Image' : wp.element.createElement('img', { src: mediaURL })
					);
				}
			})
		);
	},
	save: function save(_ref8) {
		var attributes = _ref8.attributes,
		    className = _ref8.className;
		var mediaID = attributes.mediaID,
		    mediaURL = attributes.mediaURL,
		    title = attributes.title,
		    content = attributes.content;


		var divStyle = {};
		if (mediaURL) {
			divStyle.backgroundImage = 'URL(' + mediaURL + ')';
		}

		return wp.element.createElement(
			'div',
			{ className: className, 'data-src': mediaURL, style: divStyle },
			wp.element.createElement(
				'h2',
				null,
				title
			),
			wp.element.createElement(
				'p',
				null,
				content
			)
		);
	}
});

registerBlockType('skulogic/container', {
	title: 'Container',
	description: 'A block that contains other blocks.',
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		backgroundColor: {
			type: 'string',
			default: '#ffffff'
		},
		textColor: {
			type: 'string',
			default: '#000000'
		}
	},
	edit: function edit(_ref9) {
		var attributes = _ref9.attributes,
		    className = _ref9.className,
		    setAttributes = _ref9.setAttributes;
		var backgroundColor = attributes.backgroundColor,
		    textColor = attributes.textColor;

		var setBackgroundColor = function setBackgroundColor(newBackgroundColor) {
			setAttributes({ backgroundColor: newBackgroundColor });
		};
		var setTextColor = function setTextColor(newTextColor) {
			setAttributes({ textColor: newTextColor });
		};
		return wp.element.createElement(
			Fragment,
			null,
			wp.element.createElement(
				InspectorControls,
				null,
				wp.element.createElement(PanelColor, {
					colorValue: backgroundColor,
					initialOpen: false,
					title: 'Background Color',
					onChange: setBackgroundColor
				}),
				wp.element.createElement(PanelColor, {
					colorValue: textColor,
					initialOpen: false,
					title: 'Text Color',
					onChange: setTextColor
				})
			),
			wp.element.createElement(
				'div',
				{ className: className, style: { backgroundColor: backgroundColor, color: textColor } },
				wp.element.createElement(InnerBlocks, null)
			)
		);
	},
	save: function save(_ref10) {
		var attributes = _ref10.attributes,
		    className = _ref10.className;
		var backgroundColor = attributes.backgroundColor,
		    textColor = attributes.textColor;

		return wp.element.createElement(
			'div',
			{ className: className, style: { backgroundColor: backgroundColor, color: textColor } },
			wp.element.createElement(
				'div',
				{ 'class': 'container' },
				wp.element.createElement(InnerBlocks.Content, null)
			)
		);
	}
});

/***/ })
/******/ ]);