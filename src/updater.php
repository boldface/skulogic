<?php

/**
 * @package Boldface\SKULogic
 */
declare( strict_types = 1 );
namespace Boldface\SKULogic;

/**
 * Class for updating the theme
 *
 * @since 0.1
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'skulogic';
  protected $repository = 'boldface/skulogic';
}
